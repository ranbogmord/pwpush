# PWPush

## Install deps
```bash
$ dep ensure
```

## Generating a master encryption key
```bash
$ go run . newkey
```

## Run the app
```bash
$ go run . -key <key from newkey command>
```

## Run as binary
```bash
$ ./pwpush -key <key>
```
