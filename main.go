package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

var key [32]byte

// Encryption functions from: https://github.com/gtank/cryptopasta/blob/master/encrypt.go
// NewEncryptionKey generates a random 256-bit key for Encrypt() and
// Decrypt(). It panics if the source of randomness fails.
func NewEncryptionKey() *[32]byte {
	key := [32]byte{}
	_, err := io.ReadFull(rand.Reader, key[:])
	if err != nil {
		panic(err)
	}
	return &key
}

// Encrypt encrypts data using 256-bit AES-GCM.  This both hides the content of
// the data and provides a check that it hasn't been altered. Output takes the
// form nonce|ciphertext|tag where '|' indicates concatenation.
func Encrypt(plaintext []byte, key *[32]byte) (ciphertext []byte, err error) {
	block, err := aes.NewCipher(key[:])
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	_, err = io.ReadFull(rand.Reader, nonce)
	if err != nil {
		return nil, err
	}

	return gcm.Seal(nonce, nonce, plaintext, nil), nil
}

// Decrypt decrypts data using 256-bit AES-GCM.  This both hides the content of
// the data and provides a check that it hasn't been altered. Expects input
// form nonce|ciphertext|tag where '|' indicates concatenation.
func Decrypt(ciphertext []byte, key *[32]byte) (plaintext []byte, err error) {
	block, err := aes.NewCipher(key[:])
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	if len(ciphertext) < gcm.NonceSize() {
		return nil, errors.New("malformed ciphertext")
	}

	return gcm.Open(nil,
		ciphertext[:gcm.NonceSize()],
		ciphertext[gcm.NonceSize():],
		nil,
	)
}

type EncryptRequest struct {
	Plaintext string `json:"plaintext"`
}

type EncryptResponse struct {
	Ciphertext string `json:"ciphertext"`
}

type EncryptedMessage struct {
	Plaintext string `json:"plaintext"`
	Expires string `json:"expires"`
}

type DecryptResponse struct {
	Plaintext string `json:"plaintext"`
}

func EncryptHandler(w http.ResponseWriter, r *http.Request) {
	req := &EncryptRequest{}
	err  := json.NewDecoder(r.Body).Decode(req)
	if err != nil {
		log.Printf("failed to decode json: %v", err)
		w.WriteHeader(400)
		return
	}
	defer r.Body.Close()

	if req.Plaintext == "" {
		w.WriteHeader(400)
		return
	}

	msg := &EncryptedMessage{}
	msg.Plaintext = req.Plaintext
	msg.Expires = time.Now().Add(time.Hour * 24).Format(time.RFC3339)

	jsonMsg := bytes.Buffer{}
	err = json.NewEncoder(&jsonMsg).Encode(msg)
	if err != nil {
		log.Printf("failed to json encode message: %v", err)
		w.WriteHeader(500)
		return
	}

	ciphertext, err := Encrypt(jsonMsg.Bytes(), &key)
	if err != nil {
		w.WriteHeader(500)
		return
	}

	b64ciphertext := base64.URLEncoding.EncodeToString(ciphertext)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&EncryptResponse{Ciphertext: b64ciphertext})
}

func DecryptHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	b64ciphertext := params["encString"]

	rawcipher, err := base64.URLEncoding.DecodeString(b64ciphertext)
	if err != nil {
		w.WriteHeader(500)
		log.Printf("failed to decode ciphertext: %v", err)
		return
	}

	plaintext, err := Decrypt(rawcipher, &key)
	if err != nil {
		w.WriteHeader(500)
		log.Printf("failed to decrypt ciphertext: %v", err)
		return
	}

	jsonMsg := bytes.NewReader(plaintext)
	msg := &EncryptedMessage{}
	err = json.NewDecoder(jsonMsg).Decode(msg)
	if err != nil {
		w.WriteHeader(500)
		log.Printf("failed to decode json message: %v", err)
		return
	}

	now := time.Now()
	exp, err := time.Parse(time.RFC3339, msg.Expires)
	if err != nil {
		w.WriteHeader(500)
		log.Printf("failed to parse expires time: %v", err)
		return
	}

	if now.After(exp) {
		w.WriteHeader(403)
		log.Printf("link is expired")
		return
	}

	w.Header().Add("Content-Type", "application/json")
	res := &DecryptResponse{Plaintext: msg.Plaintext}
	json.NewEncoder(w).Encode(res)
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadFile("public/index.html")
	if err != nil {
		w.WriteHeader(500)
		log.Fatalf("failed to read index file: %v", err)
		return
	}

	w.Write(data)
}

func main() {
	if len(os.Args) > 1 && os.Args[1] == "newkey" {
		newkey := NewEncryptionKey()

		fmt.Printf("New key: %s", base64.URLEncoding.EncodeToString(newkey[:]))
		return
	}

	base64key := flag.String("key", "", "Encryption key")
	flag.Parse()

	if *base64key == "" {
		log.Fatalf("invalid key")
	}

	rawkey, err := base64.URLEncoding.DecodeString(*base64key)
	if err != nil {
		log.Fatalf("invalid key")
	}

	copy(key[:], rawkey[:32])

	r := mux.NewRouter()

	r.HandleFunc("/", IndexHandler).Methods("GET")
	r.HandleFunc("/api/encrypt", EncryptHandler).Methods("POST")
	r.HandleFunc("/api/link/{encString}", DecryptHandler).Methods("GET")

	http.ListenAndServe(":8000", r)
}
